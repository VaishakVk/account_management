import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('account_types', (table) => {
        table.increments();
        table.string('name', 55).unique();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.timestamp('deleted_at');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('account_types');
}
