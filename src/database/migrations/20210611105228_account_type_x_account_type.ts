import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('account_type_x_account_type', (table) => {
        table.increments();
        table
            .integer('parent_id')
            .unsigned()
            .references('id')
            .inTable('account_types');
        table
            .integer('child_id')
            .unsigned()
            .references('id')
            .inTable('account_types');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.timestamp('deleted_at');
        table.unique(['parent_id', 'child_id']);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('account_type_x_account_type');
}
