import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('accounts', (table) => {
        table.increments();
        table.string('email').unique();
        table.string('password');
        table.string('first_name');
        table.string('last_name');
        table.string('status');
        table.string('token');
        table.string('webhook_response', 8192);
        table.timestamp('expires_at');
        table.timestamp('activated_at');
        table.timestamp('last_logged_in');
        table.string('forget_password_token');
        table.timestamp('forget_password_token_expiry');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.timestamp('deleted_at');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('accounts');
}
