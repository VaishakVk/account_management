import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.table('accounts', (table) => {
        table
            .integer('parent_id')
            .unsigned()
            .references('id')
            .inTable('accounts');

        table
            .integer('account_type_id')
            .unsigned()
            .references('id')
            .inTable('account_types');

        table.string('invite_code');
        table.timestamp('invite_expiry');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable('accounts', (table) => {
        table.dropForeign('parent_id');
        table.dropColumn('parent_id');
        table.dropForeign('account_type_id');
        table.dropColumn('account_type_id');
        table.dropColumn('invite_code');
        table.dropColumn('invite_expiry');
    });
}
