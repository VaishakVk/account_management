import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('downloads', (table) => {
        table.increments();
        table
            .integer('user_id')
            .unsigned()
            .references('id')
            .inTable('accounts');
        table.string('url_downloaded');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.timestamp('deleted_at');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('downloads');
}
