import * as mysql from 'mysql';
import { getDatabaseConfig } from './config';

export default class MySqlDatabase {
    private static _instance: mysql.Connection;

    public static async getConnection() {
        if (this._instance) {
            return this._instance;
        } else {
            this._instance = await this.setupDatabase();
            return this._instance;
        }
    }

    static async setupDatabase(): Promise<mysql.Connection> {
        return new Promise<mysql.Connection>((resolve, reject) => {
            const connection = mysql.createConnection(getDatabaseConfig());
            connection.connect((error) => {
                if (error) {
                    reject(error);
                    return;
                }
                resolve(connection);
            });
        });
    }
}
