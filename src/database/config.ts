//MySQL Configuration
export function getDatabaseConfig() {
    if (process.env.NODE_ENV == 'test') {
        return {
            user: process.env.TEST_MYSQL_USER,
            password: process.env.TEST_MYSQL_PASSWORD,
            host: process.env.TEST_MYSQL_HOST,
            database: process.env.TEST_MYSQL_DATABASE,
            port: Number(process.env.TEST_MYSQL_PORT),
            multipleStatements: true,
        };
    }
    return {
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        host: process.env.MYSQL_HOST,
        database: process.env.MYSQL_DATABASE,
        port: Number(process.env.MYSQL_PORT),
    };
}
