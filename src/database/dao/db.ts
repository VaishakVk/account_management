import * as mysql from 'mysql';
import MySqlDatabase from '../connect';
import { InterfaceAccount } from '../../models/Account';
import { InterfaceDownloads } from '../../models/Downloads';
import { InterfaceAccountType } from '../../models/AccountType';

export default class DatabaseDaoService {
    /**
     * Generic function to performing queries using try/catch
     * @param sqlString
     * @param params
     */
    static async query(sqlString: string, params: any[]): Promise<any> {
        try {
            return new Promise(async (resolve, reject) => {
                const connection: mysql.Connection = await MySqlDatabase.getConnection();
                connection.query(sqlString, params, (err, result) => {
                    if (err) reject(err);
                    else resolve(result);
                });
            });
        } catch (err) {
            throw new Error(err);
        }
    }

    static async insertIntoAccount(
        payload: InterfaceAccount
    ): Promise<mysql.OkPacket> {
        return new Promise(async (resolve, reject) => {
            const connection: mysql.Connection = await MySqlDatabase.getConnection();
            const {
                email,
                first_name,
                last_name,
                password,
                webhook_response,
                token,
                expires_at,
                status,
                account_type_id,
                invite_code,
                invite_expiry,
                parent_id,
            } = payload;
            connection.query(
                `INSERT INTO accounts(email,first_name,last_name,password,webhook_response,token,
                    expires_at,status,account_type_id, invite_code, invite_expiry, parent_id) 
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`,
                [
                    email,
                    first_name,
                    last_name,
                    password,
                    webhook_response,
                    token,
                    expires_at,
                    status,
                    account_type_id,
                    invite_code,
                    invite_expiry,
                    parent_id,
                ],
                (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                }
            );
        });
    }

    static async insertIntoDownload(
        payload: InterfaceDownloads
    ): Promise<mysql.OkPacket> {
        return new Promise(async (resolve, reject) => {
            const connection: mysql.Connection = await MySqlDatabase.getConnection();
            const { user_id, url_downloaded } = payload;

            connection.query(
                `INSERT INTO downloads(user_id, url_downloaded) 
                VALUES (?,?)`,
                [user_id, url_downloaded],
                (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                }
            );
        });
    }

    static async insertIntoAccountType(
        payload: InterfaceAccountType
    ): Promise<mysql.OkPacket> {
        return new Promise(async (resolve, reject) => {
            const connection: mysql.Connection = await MySqlDatabase.getConnection();
            const { name } = payload;

            connection.query(
                `INSERT INTO account_types(name) 
                VALUES (?)`,
                [name],
                (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                }
            );
        });
    }

    static async insertIntoAccountMapping(
        parentId: number,
        childId: number
    ): Promise<mysql.OkPacket> {
        return new Promise(async (resolve, reject) => {
            const connection: mysql.Connection = await MySqlDatabase.getConnection();

            connection.query(
                `INSERT INTO account_type_x_account_type(parent_id, child_id) 
                VALUES (?, ?)`,
                [parentId, childId],
                (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                }
            );
        });
    }
}
