import * as dotenv from 'dotenv';
dotenv.config({ path: '../../.env' });
import { getDatabaseConfig } from './config';

const config = {
    client: 'mysql',
    connection: getDatabaseConfig(),
};
export default config;
