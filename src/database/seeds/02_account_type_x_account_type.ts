import { Knex } from 'knex';
import { enterprise } from '../../constants';

export function seed(knex: Knex) {
    return knex
        .from('account_types')
        .where({ name: enterprise })

        .then(function (data) {
            return knex('account_type_x_account_type').insert([
                { parent_id: data[0].id, child_id: data[0].id },
            ]);
        });
}
