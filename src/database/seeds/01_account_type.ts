import { Knex } from 'knex';
import { enterprise, solo } from '../../constants';

export function seed(knex: Knex) {
    return knex('account_type_x_account_type')
        .del()
        .then(() => knex('account_types').del())
        .then(function () {
            return knex('account_types').insert([
                { name: enterprise },
                { name: solo },
            ]);
        });
}
