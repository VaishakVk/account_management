import {
    EmailNameTemplate,
    EmailResetPasswordTemplate,
    EmailVerificationTemplate,
    EmailInviteTemplate,
} from '../../models/Email';

import * as sgMail from '@sendgrid/mail';
import { EmailTemplates } from '../../constants';

export class MailService {
    static sendEmail(
        to: string,
        category: EmailTemplates,
        data:
            | EmailVerificationTemplate
            | EmailResetPasswordTemplate
            | EmailNameTemplate
            | EmailInviteTemplate
    ): Promise<void> {
        return new Promise((resolve, reject) => {
            sgMail.setApiKey(process.env.SENDGRID_KEY);
            const templateId = category;
            const msg = {
                to,
                from: 'noreply@vanspace3d.com',
                templateId,
                dynamic_template_data: data,
            };
            sgMail.send(msg, false, (error) => {
                if (error) {
                    console.dir(error, { depth: null });
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    }
}
