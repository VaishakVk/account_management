import * as jwt from 'jsonwebtoken';

export default class JWT {
    /**
     * @name sign
     * @param payload string
     * @returns string
     */
    static async sign(payload: object): Promise<string> {
        if (!payload) throw { message: 'Paylaod is required' };
        const key = process.env.JWT_KEY;
        return jwt.sign(payload, key);
    }

    /**
     * @name decode
     * @param token string
     * @returns string
     */
    static async decode(token: string): Promise<object | string> {
        if (!token) throw { message: 'Token is required' };
        if (token.includes('Bearer'))
            token = token.replace('Bearer', '').trim();
        const key = process.env.JWT_KEY;
        return jwt.verify(token, key);
    }
}
