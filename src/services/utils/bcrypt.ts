import * as bcrypt from 'bcryptjs';

export default class BcryptService {
    /**
     * @name hash
     * @param data string
     * @returns string
     */
    static async hash(data: string): Promise<string> {
        if (!data) throw 'Data is required';
        const salt = await bcrypt.genSalt(Number(process.env.SALT_ROUNDS));
        const hashed = await bcrypt.hash(data, salt);
        return hashed;
    }

    /**
     * @name compareHash
     * @param plainText string
     * @param hash string
     * @returns boolean
     */
    static async compareHash(
        plainText: string,
        hash: string
    ): Promise<boolean> {
        const valid = await bcrypt.compare(plainText, hash);
        return valid;
    }
}
