import * as express from 'express';
import { StatusCode } from '../../constants';

export class ResponseService {
    static successResponse(
        res: express.Response,
        statusCode: number,
        message: any
    ) {
        res.status(statusCode).send({ success: true, message });
    }

    static errorResponse(res: express.Response, statusCode: number, err: any) {
        console.log(err);
        res.status(statusCode || StatusCode.InternalServerError).send({
            success: false,
            error: err.message || err,
        });
    }
}
