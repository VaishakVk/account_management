import { StatusCode } from '../../constants';
import DBDaoService from '../../database/dao/db';
import { InterfaceAccountMapping } from '../../models/AccountMapping';
import { InterfaceAccountType } from '../../models/AccountType';

export class AccountTypeRepo {
    /**
     * @name insertAccountType
     * @param payload
     * @returns id of the inserted user
     */
    static async insertAccountType(
        name: string
    ): Promise<Record<string, number>> {
        const insertPayload: InterfaceAccountType = {
            name,
        };
        const result = await DBDaoService.insertIntoAccountType(insertPayload);

        return { id: result.insertId };
    }

    /**
     * @name getAccountTypeById
     * @param id
     * @returns InterfaceAccountType
     */
    static async getAccountTypeById(id): Promise<InterfaceAccountType> {
        const selectQuery = 'SELECT * FROM account_types WHERE id = ? LIMIT 1';

        const selectParams = [id];
        const result: InterfaceAccountType[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) return result[0];
        else return null;
    }

    /**
     * @name getAccountTypeByName
     * @param name
     * @returns InterfaceAccountType
     */
    static async getAccountTypeByName(
        name: string
    ): Promise<InterfaceAccountType> {
        const selectQuery =
            'SELECT * FROM account_types WHERE name = ? LIMIT 1';

        const selectParams = [name];
        const result: InterfaceAccountType[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) return result[0];
        else return null;
    }

    /**
     * @name updateAccountType
     * @param id
     * @param name
     * @returns Updated InterfaceAccountType
     */
    static async updateAccountType(id, name): Promise<InterfaceAccountType> {
        const updateQuery =
            'UPDATE account_types SET  updated_at = ?, name= ?  WHERE id = ?';
        const updateParams = [new Date(), name, id];
        await DBDaoService.query(updateQuery, updateParams);
        return this.getAccountTypeById(id);
    }

    /**
     * @name createMapping
     * @param parentId
     * @param childId
     * @returns Inserted Record ID
     */
    static async createMapping(
        parentId,
        childId
    ): Promise<Record<string, number>> {
        const selectQuery = 'SELECT * FROM account_types WHERE id IN (?, ?)';

        const selectParams = [parentId, childId];
        const result: InterfaceAccountType[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        const parentRecord = result.filter((rec) => rec.id == parentId)[0];
        const childRecord = result.filter((rec) => rec.id == childId)[0];
        if (!parentRecord || !childRecord)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'Account type does not exist',
            };

        const insertedResults = await DBDaoService.insertIntoAccountMapping(
            parentId,
            childId
        );
        return { id: insertedResults.insertId };
    }

    /**
     * @name isAccountTypeValid
     * @param parentAccountId number
     * @param childAccountId number
     */
    static async isAccountTypeValid(
        parentAccountId: number,
        childAccountId: number
    ): Promise<boolean> {
        const selectQuery =
            'SELECT * FROM account_type_x_account_type WHERE parent_id = ? AND child_id = ? LIMIT 1';

        const selectParams = [parentAccountId, childAccountId];
        const result: InterfaceAccountMapping = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result) return true;
        else return false;
    }
}
