import { check } from 'express-validator';

export function createAccountTypeValidators() {
    return [check('name').isString().withMessage('Name is missing')];
}

export function updateAccountTypeValidators() {
    return [
        check('name').isString().withMessage('Name is missing'),
        check('id').isNumeric().withMessage('ID is missing'),
    ];
}

export function createMappingValidators() {
    return [
        check('parent_id').isNumeric().withMessage('Parent ID is missing'),
        check('child_id').isNumeric().withMessage('Child ID is missing'),
    ];
}
