import * as express from 'express';
import { validationResult } from 'express-validator';
import { StatusCode } from '../../constants';
import { ResponseService } from '../response';
import { AccountTypeRepo } from './accountTypeRepo';

export class AccountTypeService {
    /**
     * @name createAccountType
     * @param req
     * @param res
     * @description Endpoint to create account type
     */
    static async createAccountType(
        req: express.Request,
        res: express.Response
    ) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { name } = req.body;
            const result = await AccountTypeRepo.insertAccountType(name);
            return ResponseService.successResponse(
                res,
                StatusCode.Created,
                result
            );
        } catch (err) {
            if (err.code == 'ER_DUP_ENTRY')
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    'Account type name is already taken'
                );
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name updateAccountType
     * @param req
     * @param res
     * @description Endpoint to update account type
     */
    static async updateAccountType(
        req: express.Request,
        res: express.Response
    ) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { name, id } = req.body;
            const result = await AccountTypeRepo.updateAccountType(id, name);
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name createMapping
     * @param req
     * @param res
     * @description Endpoint to create mapping between account types
     */
    static async createMapping(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { parent_id, child_id } = req.body;
            const result = await AccountTypeRepo.createMapping(
                parent_id,
                child_id
            );
            return ResponseService.successResponse(
                res,
                StatusCode.Created,
                result
            );
        } catch (err) {
            if (err.code == 'ER_DUP_ENTRY')
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    'Combination already exists'
                );

            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }
}
