import * as express from 'express';
import { StatusCode } from '../../constants';
import { InterfaceAccount } from '../../models/Account';
import { AccountRepo } from '../account/accountRepo';
import JWT from '../utils/jwt';

declare module 'express-serve-static-core' {
    interface Request {
        user?: InterfaceAccount;
    }
}

/**
 * @name isAuthenticated
 * @param req
 * @param _
 * @param next
 * @description Middleware to parse request header and add user object to request
 */
export async function isAuthenticated(
    req: express.Request,
    _,
    next: express.NextFunction
) {
    try {
        let token = req.headers.authorization;
        if (token.includes('Bearer ')) token = token.replace('Bearer ', '');
        if (!token)
            next({
                statusCode: StatusCode.Unauthorized,
                message: 'Invalid token',
            });
        const payload = await JWT.decode(token);
        if (payload['expires'] < new Date().getTime())
            next({
                statusCode: StatusCode.Unauthorized,
                message: 'Invalid Token',
            });
        const user = await AccountRepo.getAccountById(payload['id']);
        if (!user)
            next({
                statusCode: StatusCode.Unauthorized,
                message: 'Invalid user',
            });
        else {
            req.user = user;
        }
        next();
    } catch (err) {
        console.log(err);
        return next({
            statusCode: StatusCode.Unauthorized,
            message: 'Invalid user',
        });
    }
}
