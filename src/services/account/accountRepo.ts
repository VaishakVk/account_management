import {
    AccountStatusEnum,
    EmailTemplates,
    externalUrl,
    StatusCode,
} from '../../constants';
import DBDaoService from '../../database/dao/db';
import { InterfaceAccount } from '../../models/Account';
import {
    EmailInviteTemplate,
    EmailNameTemplate,
    EmailResetPasswordTemplate,
    EmailVerificationTemplate,
} from '../../models/Email';
import { AccountTypeRepo } from '../accountType/accountTypeRepo';
import BcryptService from '../utils/bcrypt';
import JWT from '../utils/jwt';
import { MailService } from '../utils/mail';
import { MiscService } from '../utils/misc';

export class AccountRepo {
    static defaultExpiryTimeOfSignup = 7 * 24 * 60;
    static defaultExpiryTimeOfLogin = 10 * 24 * 60;
    /**
     * @name insertUser
     * @param payload
     * @returns id of the inserted user
     */
    static async insertUser(payload): Promise<Record<string, number>> {
        const { email, first_name, last_name } = payload.customer;
        const { account_type } = payload;
        const token = MiscService.generateToken();
        const expires_at = new Date(
            new Date().getTime() + this.defaultExpiryTimeOfSignup * 60 * 1000
        );
        const accountData = await AccountTypeRepo.getAccountTypeByName(
            account_type
        );
        const insertPayload: InterfaceAccount = {
            email,
            first_name,
            last_name,
            token,
            expires_at,
            account_type_id: accountData.id,
            webhook_response: JSON.stringify(payload),
            status: AccountStatusEnum.inactive,
        };
        const result = await DBDaoService.insertIntoAccount(insertPayload);
        const updateQuery =
            'UPDATE accounts SET parent_id = ?, updated_at = ? WHERE id = ?';
        const updateParams = [result.insertId, new Date(), result.insertId];
        await DBDaoService.query(updateQuery, updateParams);
        const emailPayload: EmailVerificationTemplate = {
            name: first_name + ' ' + last_name,
            verification: {
                code: token,
                url: `${externalUrl}?verify-code=${token}`,
            },
        };
        MailService.sendEmail(
            email,
            EmailTemplates.emailVerification,
            emailPayload
        );

        return { id: result.insertId };
    }

    /**
     * @name inviteUser
     * @param payload
     * @returns id of the inserted user
     */
    static async inviteUser(
        email: string,
        first_name: string,
        last_name: string,
        accountTypeId: number,
        userId: number,
        parentUserId: number
    ): Promise<Record<string, number>> {
        const token = MiscService.generateToken();
        const expires_at = new Date(
            new Date().getTime() + this.defaultExpiryTimeOfSignup * 60 * 1000
        );
        if (userId != parentUserId)
            throw {
                statusCode: StatusCode.Forbidden,
                message: 'You cannot invite other users',
            };
        const insertPayload: InterfaceAccount = {
            email,
            invite_code: token,
            first_name,
            last_name,
            invite_expiry: expires_at,
            account_type_id: accountTypeId,
            status: AccountStatusEnum.inactive,
            parent_id: userId,
        };

        const result = await DBDaoService.insertIntoAccount(insertPayload);

        const emailPayload: EmailInviteTemplate = {
            name: first_name + ' ' + last_name,
            invite: {
                code: token,
                url: `${externalUrl}?accept-invite=${token}`,
            },
        };
        MailService.sendEmail(email, EmailTemplates.invite, emailPayload);

        return { id: result.insertId };
    }

    /**
     * @name verifyInviteCode
     * @param code string
     * @returns account details
     */
    static async verifyInviteCode(code: string): Promise<InterfaceAccount> {
        const selectQuery =
            'SELECT * FROM accounts WHERE invite_code = ? LIMIT 1';
        const params = [code];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            params
        );
        const userData = result[0];
        if (!userData)
            throw {
                stausCode: StatusCode.NotFound,
                message: 'Invite code not found',
            };
        if (userData.invite_expiry < new Date())
            throw {
                statusCode: StatusCode.Unauthorized,
                message: 'Invite has expired',
            };
        if (userData.status == AccountStatusEnum.active)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'User is already active',
            };

        delete userData.password;
        return userData;
    }

    /**
     * @name verifySignupCode
     * @param code string
     * @returns account details
     */
    static async verifySignupCode(code: string): Promise<InterfaceAccount> {
        const selectQuery = 'SELECT * FROM accounts WHERE token = ? LIMIT 1';
        const params = [code];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            params
        );
        const userData = result[0];
        if (!userData)
            throw {
                stausCode: StatusCode.NotFound,
                message: 'Token not found',
            };
        if (userData.expires_at < new Date())
            throw {
                statusCode: StatusCode.Unauthorized,
                message: 'Token is expired',
            };
        if (userData.status == AccountStatusEnum.active)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'User is already active',
            };

        delete userData.password;
        return userData;
    }

    /**
     * @name getAccountById
     * @param userId number
     * @returns InterfaceAccount
     */
    static async getAccountById(
        userId: number,
        includePassword?: boolean
    ): Promise<InterfaceAccount> {
        let selectQuery;
        if (includePassword)
            selectQuery = 'SELECT * FROM accounts WHERE id = ? LIMIT 1';
        else
            selectQuery = `SELECT id, email, first_name, last_name, parent_id, 
            account_type_id, status, token, expires_at, forget_password_token, 
            forget_password_token_expiry, activated_at, last_logged_in, webhook_response, created_at, updated_at, deleted_at 
         FROM accounts WHERE id = ? LIMIT 1;`;
        const selectParams = [userId];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) return result[0];
        else return null;
    }

    /**
     * @name getMultipleAccountsById
     * @param userId number
     * @returns InterfaceAccount
     */
    static async getMultipleAccountsById(
        userIds: number[],
        includePassword?: boolean
    ): Promise<InterfaceAccount[]> {
        let selectQuery;
        if (includePassword)
            selectQuery = 'SELECT * FROM accounts WHERE id IN (?) ';
        else
            selectQuery = `SELECT id, email, first_name, last_name, status, token, expires_at, forget_password_token, 
            forget_password_token_expiry, activated_at, last_logged_in, webhook_response, created_at, updated_at, deleted_at 
         FROM accounts WHERE id IN (?) ;`;
        const selectParams = [userIds];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        return result;
    }

    /**
     * @name getAccountByEmail
     * @param email string
     * @returns InterfaceAccount
     */
    static async getAccountByEmail(
        email: string,
        includePassword?: boolean
    ): Promise<InterfaceAccount> {
        let selectQuery;
        if (includePassword)
            selectQuery =
                'SELECT * FROM accounts WHERE LOWER(email) = ? LIMIT 1';
        else
            selectQuery = `SELECT id, email, first_name, last_name, status, token, expires_at, forget_password_token, 
            forget_password_token_expiry, activated_at, last_logged_in, webhook_response, created_at, updated_at, deleted_at 
         FROM accounts WHERE LOWER(email) = ? LIMIT 1;`;
        const selectParams = [email.toLowerCase()];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) return result[0];
        else return null;
    }

    /**
     * @name getAccountWithAccountTypeByEmail
     * @param email string
     * @returns InterfaceAccount
     */
    static async getAccountWithAccountTypeByEmail(
        email: string,
        includePassword?: boolean
    ) {
        const selectQuery = `SELECT child.*, 
                            parent.account_type_id as parent_account_type_id,
                            parent.first_name as parent_first_name,
                            parent.last_name as parent_last_name,
                            parent.status as parent_status,
                            parent_account_type.name as parent_account,
                            child_account_type.name as child_account
                        FROM accounts child
                            , accounts parent
                            , account_types child_account_type
                            , account_types parent_account_type
                        WHERE LOWER(child.email) = ? 
                          AND child.parent_id = parent.id
                          AND child.account_type_id = child_account_type.id
                          AND parent.account_type_id = parent_account_type.id
                        LIMIT 1`;

        const selectParams = [email.toLowerCase()];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) {
            if (!includePassword) delete result[0].password;
            return result[0];
        } else return null;
    }

    /**
     * @name getAccountWithAccountTypeByID
     * @param id number
     */
    static async getAccountWithAccountTypeByID(
        id: number,
        includePassword?: boolean
    ) {
        const selectQuery = `SELECT child.*, 
                            parent.account_type_id as parent_account_type_id,
                            parent.first_name as parent_first_name,
                            parent.last_name as parent_last_name,
                            parent_account_type.name as parent_account,
                            child_account_type.name as child_account
                        FROM accounts child
                            , accounts parent
                            , account_types child_account_type
                            , account_types parent_account_type
                        WHERE child.id = ? 
                          AND child.parent_id = parent.id
                          AND child.account_type_id = child_account_type.id
                          AND parent.account_type_id = parent_account_type.id
                        LIMIT 1`;

        const selectParams = [id];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        if (result && result.length == 1) {
            if (!includePassword) delete result[0].password;
            return result[0];
        } else return null;
    }

    /**
     * @name activateAndUpdatePassword
     * @param userId number
     * @param password string
     * @param onlyPassword boolean
     * @returns InterfaceAccount
     */
    static async activateAndUpdatePassword(
        userId: number,
        password: string,
        onlyPassword?: boolean // This is to update only password and when user is active
    ): Promise<InterfaceAccount> {
        const userData = await this.getAccountById(userId);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'User not found',
            };
        // if onlyPassword is false, it indicates the account is inactive
        if (userData.status == AccountStatusEnum.active && !onlyPassword)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'User is already active',
            };
        // if onlyPassword is true, it indicates the account is active
        if (userData.status == AccountStatusEnum.inactive && onlyPassword) {
            throw {
                statusCode: StatusCode.Forbidden,
                message: 'User is inactive',
            };
        }
        const hashedPassword = await BcryptService.hash(password);
        let updateQuery, updateParams;
        if (onlyPassword) {
            updateQuery =
                'UPDATE accounts SET password = ?, updated_at = ? WHERE id = ?';
            updateParams = [hashedPassword, new Date(), userId];
        } else {
            updateQuery = `UPDATE accounts SET password = ?, status = ?, activated_at = ?
                , token = null, expires_at = null, invite_code = null, invite_expiry = null, 
                updated_at = ? WHERE id = ?`;
            updateParams = [
                hashedPassword,
                AccountStatusEnum.active,
                new Date(),
                new Date(),
                userId,
            ];
        }
        await DBDaoService.query(updateQuery, updateParams);
        const updatedResult = await this.getAccountById(userId);
        if (!onlyPassword) {
            const emailPayload: EmailNameTemplate = {
                name: userData.first_name + ' ' + userData.last_name,
            };
            await MailService.sendEmail(
                userData.email,
                EmailTemplates.welcome,
                emailPayload
            );
        }
        return updatedResult;
    }

    /**
     * @name login
     * @param email string
     * @param password string
     * @returns string
     */
    static async login(email: string, password: string): Promise<string> {
        const userData = await this.getAccountWithAccountTypeByEmail(
            email,
            true
        );
        console.log(userData);
        if (!userData)
            throw {
                statusCode: StatusCode.Unauthorized,
                message: 'Email or password did not match',
            };
        const isPasswordMatch = await BcryptService.compareHash(
            password,
            userData.password
        );
        if (!isPasswordMatch)
            throw {
                statusCode: StatusCode.Unauthorized,
                message: 'Email or password did not match',
            };

        if (userData.status == AccountStatusEnum.inactive)
            throw {
                statusCode: StatusCode.Unauthorized,
                message: 'Account is not activated',
            };
        if (userData.parent_status == AccountStatusEnum.inactive)
            throw {
                statusCode: StatusCode.Forbidden,
                message: 'Parent is inactive',
            };
        const payload = {
            id: userData.id,
            expires:
                new Date().getTime() +
                this.defaultExpiryTimeOfLogin * 60 * 1000,
        };
        const token = await JWT.sign(payload);
        const updateQuery =
            'UPDATE accounts SET last_logged_in = ?, updated_at = ?  WHERE id = ?';
        const updateParams = [new Date(), new Date(), userData.id];
        await DBDaoService.query(updateQuery, updateParams);
        return token;
    }

    /**
     * @name deactivateAccount
     * @param userId number
     */
    static async deactivateAccount(
        userId: number,
        name: string,
        email
    ): Promise<void> {
        const updateQuery =
            'UPDATE accounts SET status = ?, updated_at = ?  WHERE id = ?';
        const updateParams = [AccountStatusEnum.inactive, new Date(), userId];
        await DBDaoService.query(updateQuery, updateParams);
        const emailPayload: EmailNameTemplate = {
            name,
        };
        await MailService.sendEmail(
            email,
            EmailTemplates.deactivated,
            emailPayload
        );
    }

    /**
     * @name deactivateAccountWithEmail
     * @param userId number
     */
    static async deactivateAccountWithEmail(email: string): Promise<void> {
        const userData = await this.getAccountByEmail(email);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'Account not found',
            };
        if (userData.status == AccountStatusEnum.inactive)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'User is already inactive',
            };
        await AccountRepo.deactivateAccount(
            userData.id,
            userData.first_name + ' ' + userData.last_name,
            userData.email
        );
    }

    /**
     * @name deactivateAccountWithId
     * @param userId number
     */
    static async deactivateAccountWithId(id: number): Promise<void> {
        const userData = await this.getAccountById(id);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'Account not found',
            };
        if (userData.status == AccountStatusEnum.inactive)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'User is already inactive',
            };
        await AccountRepo.deactivateAccount(
            userData.id,
            userData.first_name + ' ' + userData.last_name,
            userData.email
        );
    }

    /**
     * @name downloadURL
     * @param userId number
     * @param downloadURL string
     * @returns inserted id
     */
    static async downloadURL(
        userId,
        downloadURL
    ): Promise<Record<string, number>> {
        const userData = await this.getAccountById(userId);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'User not found',
            };
        const downloadData = await DBDaoService.insertIntoDownload({
            user_id: userId,
            url_downloaded: downloadURL,
        });
        return { id: downloadData.insertId };
    }

    /**
     * @name getDownloadByUserId
     * @param userId number
     * @returns count of records
     */
    static async getDownloadByUserId(userId): Promise<Record<string, number>> {
        const userData = await this.getAccountById(userId);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'User not found',
            };
        const selectQuery =
            'SELECT count(*) as count FROM downloads WHERE user_id = ?';
        const selectParams = [userId];
        const downloadData = await DBDaoService.query(
            selectQuery,
            selectParams
        );
        return downloadData[0];
    }

    /**
     * @name generateForgetPassword
     * @param email string
     */
    static async generateForgetPassword(email: string) {
        const userData = await this.getAccountByEmail(email);
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'User does not exist',
            };
        const token = MiscService.generateToken();
        const expires = new Date(new Date().getTime() + 60 * 60 * 1000);
        const updateQuery =
            'UPDATE accounts SET forget_password_token = ?, forget_password_token_expiry = ?  WHERE id = ?';
        const updateParams = [token, expires, userData.id];
        await DBDaoService.query(updateQuery, updateParams);
        const emailPayload: EmailResetPasswordTemplate = {
            name: userData.first_name + ' ' + userData.last_name,
            url: `${externalUrl}reset-password?code=${token}`,
        };
        await MailService.sendEmail(
            email,
            EmailTemplates.resetPassword,
            emailPayload
        );
    }

    /**
     * @name verifyForgetPasswordToken
     * @param token string
     */
    static async verifyForgetPasswordToken(
        token: string
    ): Promise<InterfaceAccount> {
        const selectQuery =
            'SELECT * FROM accounts WHERE forget_password_token = ? LIMIT 1';
        const params = [token];
        const result: InterfaceAccount[] = await DBDaoService.query(
            selectQuery,
            params
        );
        const userData = result[0];
        if (!userData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'Token not found',
            };
        if (userData.forget_password_token_expiry < new Date())
            throw {
                statusCode: StatusCode.Forbidden,
                message: 'Token expired',
            };
        delete userData.password;
        return userData;
    }

    /**
     * @name linkParentAccount
     * @param userId number
     * @param parentId number
     * @param accountTypeId number
     */
    static async linkParentAccount(
        userId: number,
        parentId: number,
        accountTypeId: number
    ): Promise<InterfaceAccount> {
        const userData = await this.getMultipleAccountsById([userId, parentId]);
        const accountData = await AccountTypeRepo.getAccountTypeById(
            accountTypeId
        );
        if (!accountData)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'Invalid Account',
            };
        const parentRecord = userData.filter((user) => user.id == parentId)[0];
        const childRecord = userData.filter((user) => user.id == userId)[0];
        if (!parentRecord || !childRecord)
            throw {
                statusCode: StatusCode.NotFound,
                message: 'User does not exist',
            };
        const isValidAccountType = await AccountTypeRepo.isAccountTypeValid(
            parentRecord.account_type_id,
            childRecord.account_type_id
        );
        if (!isValidAccountType)
            throw {
                statusCode: StatusCode.BadRequest,
                message: 'Invalid Combination',
            };
        const updateQuery = `UPDATE accounts SET parent_id = ?, account_type_id = ? , 
             updated_at = ? 
            WHERE id = ?;`;
        const updateParams = [parentId, accountTypeId, new Date(), userId];
        await DBDaoService.query(updateQuery, updateParams);
        return this.getAccountWithAccountTypeByID(userId);
    }
}
