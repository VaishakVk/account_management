import { check } from 'express-validator';

export function successWebhookValidators() {
    return [
        check('customer').isObject().withMessage('Customer object is missing'),
        check('customer.first_name')
            .isString()
            .withMessage('First Name is not provided'),
        check('customer.last_name')
            .isString()
            .withMessage('Last Name is not provided'),
        check('customer.email')
            .isEmail()
            .withMessage('Email is not provided or invalid'),
    ];
}

export function inviteValidators() {
    return [
        check('first_name')
            .isString()
            .withMessage('First Name is not provided'),
        check('last_name').isString().withMessage('Last Name is not provided'),
        check('email')
            .isEmail()
            .withMessage('Email is not provided or invalid'),
        check('account_type_id')
            .isNumeric()
            .withMessage('Account Type ID is not a number or provided'),
    ];
}

export function activateAccountValidators() {
    return [
        check('user_id').isNumeric().withMessage('User ID should be numeric'),
        check('password')
            .isString()
            .isLength({ min: 6 })
            .withMessage('Password should contain atleaset 6 characters'),
    ];
}
export function updatePasswordValidators() {
    return [
        check('password')
            .isString()
            .isLength({ min: 6 })
            .withMessage('Password should contain atleaset 6 characters'),
    ];
}

export function downloadValidators() {
    return [check('url').isString().isURL().withMessage('Invalid URL')];
}

export function loginAccountValidators() {
    return [
        check('email').isEmail().withMessage('Invalid Email format'),
        check('password').isString(),
    ];
}

export function linkParentAccountValidators() {
    return [
        check('user_id').isNumeric().withMessage('User ID is not provided'),
        check('account_type_id')
            .isNumeric()
            .withMessage('Account Type ID is not provided'),
    ];
}
