import * as express from 'express';
import { validationResult } from 'express-validator';
import { StatusCode } from '../../constants';
import { ResponseService } from '../response';
import { AccountRepo } from './accountRepo';
export class AccountService {
    /**
     * @name paymentWebhook
     * @param req
     * @param res
     * @description Endpoint for successful payment webhook
     */
    static async paymentWebhook(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const result = await AccountRepo.insertUser({
                ...req.body,
                account_type: 'enterprise',
            });
            return ResponseService.successResponse(
                res,
                StatusCode.Created,
                result
            );
        } catch (err) {
            if (err.code == 'ER_DUP_ENTRY')
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    'Email is already taken'
                );

            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name invite
     * @param req
     * @param res
     * @description Endpoint for successful payment webhook
     */
    static async invite(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { first_name, last_name, email, account_type_id } = req.body;
            const result = await AccountRepo.inviteUser(
                email,
                first_name,
                last_name,
                account_type_id,
                req.user.id,
                req.user.parent_id
            );
            return ResponseService.successResponse(
                res,
                StatusCode.Created,
                result
            );
        } catch (err) {
            if (err.code == 'ER_DUP_ENTRY')
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    'Email is already taken'
                );
            else if (err.code && err.code.includes('ER_NO_REFERENCED_ROW'))
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    'Invalid Account Type'
                );
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name refundWebhook
     * @param req
     * @param res
     * @description Endpoint for refund webhook
     */
    static async refundWebhook(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            await AccountRepo.deactivateAccountWithEmail(
                req.body.customer.email
            );
            return ResponseService.successResponse(
                res,
                StatusCode.Ok,
                'Deactivated'
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name verifySignup
     * @param req
     * @param res
     * @description Verify signup - Inputs a token and validates
     */
    static async verifySignup(req: express.Request, res: express.Response) {
        try {
            const result = await AccountRepo.verifySignupCode(
                req.params.tokenId
            );
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name verifyInvite
     * @param req
     * @param res
     * @description Verify signup - Inputs a token and validates
     */
    static async verifyInvite(req: express.Request, res: express.Response) {
        try {
            const result = await AccountRepo.verifyInviteCode(
                req.params.tokenId
            );
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name getProfile
     * @param req
     * @param res
     * @description Gets details of user profile
     */
    static async getProfile(req: express.Request, res: express.Response) {
        try {
            const result = await AccountRepo.getAccountById(req.user.id);
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name activateAccount
     * @param req
     * @param res
     * @description Endpoint to activate account
     */
    static async activateAccount(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { user_id, password } = req.body;
            const result = await AccountRepo.activateAndUpdatePassword(
                user_id,
                password
            );
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name updatePassword
     * @param req
     * @param res
     * @description Endpoint to update Password
     */
    static async updatePassword(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { password } = req.body;
            const result = await AccountRepo.activateAndUpdatePassword(
                req.user.id,
                password,
                true
            );
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name login
     * @param req
     * @param res
     * @returns Endpoint to login
     */
    static async login(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { email, password } = req.body;
            const result = await AccountRepo.login(email, password);
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name downloadURL
     * @param req
     * @param res
     * @description Endpoint to trigger when url is downloaded
     */
    static async downloadUrl(req: express.Request, res: express.Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const { url } = req.body;
            const result = await AccountRepo.downloadURL(req.user.id, url);
            return ResponseService.successResponse(
                res,
                StatusCode.Created,
                result
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name getDownloadStats
     * @param req
     * @param res
     * @description Endpoint to get Download stats
     */
    static async getDownloadStats(req: express.Request, res: express.Response) {
        try {
            const result = await AccountRepo.getDownloadByUserId(req.user.id);
            return ResponseService.successResponse(res, StatusCode.Ok, result);
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name deactivate
     * @param req
     * @param res
     * @returns Endpoint to deactivate Acoount
     */
    static async deactivate(req: express.Request, res: express.Response) {
        try {
            const userId = req.user.id;
            await AccountRepo.deactivateAccountWithId(userId);
            return ResponseService.successResponse(
                res,
                StatusCode.Ok,
                'Deactivated'
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name generateForgetPassword
     * @param req
     * @param res
     * @returns Endpoint to send forget password email
     */
    static async generateForgetPassword(
        req: express.Request,
        res: express.Response
    ) {
        try {
            await AccountRepo.generateForgetPassword(req.params.email);
            return ResponseService.successResponse(
                res,
                StatusCode.Ok,
                'Mail sent'
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name verifyForgetPassword
     * @param req
     * @param res
     * @returns Endpoint to verify forget password
     */
    static async verifyForgetPassword(
        req: express.Request,
        res: express.Response
    ) {
        try {
            const userData = await AccountRepo.verifyForgetPasswordToken(
                req.params.token
            );
            return ResponseService.successResponse(
                res,
                StatusCode.Ok,
                userData
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }

    /**
     * @name linkParentAccount
     * @param req
     * @param res
     * @returns Endpoint to verify forget password
     */
    static async linkParentAccount(
        req: express.Request,
        res: express.Response
    ) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return ResponseService.errorResponse(
                    res,
                    StatusCode.BadRequest,
                    errors.array()
                );
            }
            const userData = await AccountRepo.linkParentAccount(
                req.body.user_id,
                req.user.id,
                req.body.account_type_id
            );
            return ResponseService.successResponse(
                res,
                StatusCode.Ok,
                userData
            );
        } catch (err) {
            return ResponseService.errorResponse(res, err.statusCode, err);
        }
    }
}
