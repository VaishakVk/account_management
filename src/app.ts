import * as express from 'express';
import { StatusCode } from './constants';
import MySqlDatabase from './database/connect';
import accountRoutes from './routes/account';
import downloadRoutes from './routes/downloads';
import accountTypeRoutes from './routes/accountType';
import { ResponseService } from './services/response';
import * as swaggerUi from 'swagger-ui-express';
import * as fs from 'fs';
import { isAuthenticated } from './services/middleware/isAuthenticated';
class App {
    public express: any;

    constructor() {
        this.express = express();
        this.express.use(express.json());
        this.mountRoutes();
        MySqlDatabase.getConnection();
    }

    private mountRoutes(): void {
        const swaggerFile = 'swagger.json';
        const swaggerData = fs.readFileSync(swaggerFile, 'utf8');
        const swaggerDocument = JSON.parse(swaggerData);
        this.express.get('/health', (_, res: express.Response) =>
            res.status(StatusCode.Ok).send('AoK')
        );
        this.express.use(
            '/api/docs',
            swaggerUi.serve,
            swaggerUi.setup(swaggerDocument)
        );
        this.express.use('/account', accountRoutes);
        this.express.use('/download', downloadRoutes);
        this.express.use('/accountType', isAuthenticated, accountTypeRoutes);
        /* eslint-disable @typescript-eslint/no-unused-vars */
        this.express.use((err, _, res, _next) =>
            ResponseService.errorResponse(res, err.statusCode, err)
        );
    }
}

export default new App().express;
