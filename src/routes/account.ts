import { Router } from 'express';
import { AccountService } from '../services/account';
import {
    activateAccountValidators,
    loginAccountValidators,
    successWebhookValidators,
    updatePasswordValidators,
    linkParentAccountValidators,
    inviteValidators,
} from '../services/account/validator';
import { isAuthenticated } from '../services/middleware/isAuthenticated';
const router = Router();

// GET route
router.get('/verify/:tokenId', AccountService.verifySignup);
router.get('/verify-invite/:tokenId', AccountService.verifyInvite);
router.get(
    '/forget-password/verify/:token',
    AccountService.verifyForgetPassword
);
router.get('/forget-password/:email', AccountService.generateForgetPassword);
router.get('/', isAuthenticated, AccountService.getProfile);

// PATCH route
router.patch(
    '/password',
    updatePasswordValidators(),
    isAuthenticated,
    AccountService.updatePassword
);

// POST route
router.post(
    '/refund',
    successWebhookValidators(),
    AccountService.refundWebhook
);
router.post(
    '/activate',
    activateAccountValidators(),
    AccountService.activateAccount
);
router.post(
    '/invite',
    inviteValidators(),
    isAuthenticated,
    AccountService.invite
);
router.post('/deactivate', isAuthenticated, AccountService.deactivate);
router.post('/login', loginAccountValidators(), AccountService.login);
router.post(
    '/link',
    linkParentAccountValidators(),
    isAuthenticated,
    AccountService.linkParentAccount
);
router.post('/', successWebhookValidators(), AccountService.paymentWebhook);

export default router;
