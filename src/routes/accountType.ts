import { Router } from 'express';
import { AccountTypeService } from '../services/accountType';
import {
    createAccountTypeValidators,
    createMappingValidators,
    updateAccountTypeValidators,
} from '../services/accountType/validator';

const router = Router();

// PATCH route
router.patch(
    '/',
    updateAccountTypeValidators(),
    AccountTypeService.updateAccountType
);

// POST route
router.post(
    '/map',
    createMappingValidators(),
    AccountTypeService.createMapping
);
router.post(
    '/',
    createAccountTypeValidators(),
    AccountTypeService.createAccountType
);

export default router;
