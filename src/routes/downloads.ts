import { Router } from 'express';
import { AccountService } from '../services/account';
import { downloadValidators } from '../services/account/validator';
import { isAuthenticated } from '../services/middleware/isAuthenticated';
const router = Router();

// POST route
router.post(
    '/',
    downloadValidators(),
    isAuthenticated,
    AccountService.downloadUrl
);

// GET route
router.get('/', isAuthenticated, AccountService.getDownloadStats);

export default router;
