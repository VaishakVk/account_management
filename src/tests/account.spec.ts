/* eslint-disable max-lines, jest/no-export */
import * as dotenv from 'dotenv';
dotenv.config();

import * as mysql from 'mysql';
import app from '../app';
import { agent as request } from 'supertest';
import { expect } from 'chai';
import MySqlDatabase from '../database/connect';
import * as sinon from 'sinon';
import { MailService } from '../services/utils/mail';

export const cleanup = () =>
    new Promise(async (resolve, reject) => {
        const connection: mysql.Connection = await MySqlDatabase.getConnection();
        connection.query(
            'SET foreign_key_checks = 0; DELETE FROM downloads; SET foreign_key_checks = 1; ',
            (err1) => {
                if (err1) reject(err1);
                connection.query(
                    'SET foreign_key_checks = 0; DELETE FROM accounts; SET foreign_key_checks = 1; ',
                    (err, res) => {
                        if (err) reject(err);
                        else resolve(res);
                    }
                );
            }
        );
    });

export const webhookRequest = {
    event: 'order.payment',
    date_unix: 1583499900,
    date_iso8601: '2020-03-06T13:05:01+00:00',
    customer: {
        first_name: 'Edward',
        last_name: 'Mann',
        checkbox_confirmation: 1,
        name: 'Edward Mann',
        email: 'mrema@mysite.com',
        ip_address: '127.0.0.1',
        client_user_agent:
            /* eslint-disable max-len */
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 affiliate',
        affiliate_name: 'Edward Mann',
        affiliate_user_id: 123,
        affiliate_email: 'edward@mann.com',
        affiliate_id: 'mremann',
    },
};

const password = 'Hello123';

export const createAccount = async () => {
    const res = await request(app).post('/account').send(webhookRequest);
    expect(res.status).to.equal(201);
    expect(res.body).not.to.be.empty;
    expect(res.body.success).to.be.true;
    expect(res.body.message.id).to.be.not.null;

    return res.body.message.id;
};

export const createDownload = async (token) => {
    const res = await request(app)
        .post('/download')
        .send({
            url: 'https://s3.com',
        })
        .set('authorization', token);
    expect(res.status).to.equal(201);
    expect(res.body).not.to.be.empty;
    expect(res.body.success).to.equal(true);
    expect(res.body.message.id).to.be.not.null;
};
export const activateAccount = async (userId) => {
    const res = await request(app).post('/account/activate').send({
        user_id: userId,
        password: password,
    });
    expect(res.status).to.equal(200);
    expect(res.body).not.to.be.empty;
    expect(res.body.success).to.be.true;
    expect(res.body.message.id).to.be.not.null;
    expect(res.body.message.status).to.equal('ACTIVE');

    return res.body.message.id;
};

export const login = async () => {
    const res = await request(app).post('/account/login').send({
        email: webhookRequest.customer.email,
        password: password,
    });
    expect(res.status).to.equal(200);
    expect(res.body).not.to.be.empty;
    expect(res.body.success).to.be.true;
    expect(res.body.message).to.be.not.null;

    return res.body.message;
};

export const createUserAndLogin = async () => {
    const userId = await createAccount();
    await activateAccount(userId);
    const token = await login();
    return { userId, token };
};

let stubMail;
describe('Accounts - /account', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        stubMail.restore();
        await cleanup();
    });
    beforeEach(() => cleanup());

    it('should create a new account', async () => {
        const res = await request(app).post('/account').send(webhookRequest);
        expect(res.status).to.equal(201);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
    });

    it('should reject when the same email is registered again', async () => {
        await createAccount();
        const res_email = await request(app)
            .post('/account')
            .send(webhookRequest);
        expect(res_email.status).to.equal(400);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(false);
        expect(res_email.body.error).to.be.not.null;
    });

    it('should get user profile', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res_email = await request(app)
            .get('/account')
            .set('authorization', token);
        expect(res_email.status).to.equal(200);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(true);
        expect(res_email.body.message.first_name).to.equal('Edward');
    });

    it('should fail when invalid token is sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res_email = await request(app)
            .get('/account')
            .set('authorization', 'token');
        expect(res_email.status).to.equal(401);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(false);
        expect(res_email.body.error).to.be.not.null;
    });

    it('should fail when token is not sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res_email = await request(app).get('/account');
        expect(res_email.status).to.equal(401);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(false);
        expect(res_email.body.error).to.be.not.null;
    });
});

describe('Accounts - /account/activate', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        stubMail.restore();
        await cleanup();
    });
    beforeEach(() => cleanup());

    it('should activate a new account', async () => {
        const userId = await createAccount();
        const res = await request(app).post('/account/activate').send({
            user_id: userId,
            password: password,
        });
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
        expect(res.body.message.status).to.equal('ACTIVE');
    });

    it('should reject when the account is already active', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res = await request(app).post('/account/activate').send({
            user_id: userId,
            password: password,
        });
        expect(res.status).to.equal(400);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should reject when the account does not exists', async () => {
        const res = await request(app).post('/account/activate').send({
            user_id: 312321,
            password: password,
        });
        expect(res.status).to.equal(404);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Accounts - /account/forget-password/:email', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        stubMail.restore();
        await cleanup();
    });
    beforeEach(() => cleanup());

    it('should send an email to user', async () => {
        await createAccount();
        const res = await request(app).get(
            '/account/forget-password/' + webhookRequest.customer.email
        );
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(true);
        expect(res.body.message).to.be.not.null;
    });

    it('should fail when email is invalid', async () => {
        await createAccount();
        const res = await request(app).get(
            '/account/forget-password/' + 'test'
        );
        expect(res.status).to.equal(404);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Accounts - /account/refund', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should deactivate an account when refund is processed', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res_email = await request(app)
            .post('/account/refund')
            .send(webhookRequest);
        expect(res_email.status).to.equal(200);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(true);
        expect(res_email.body.message).to.equal('Deactivated');
    });

    it('should error when an inactive account is again being made inactive', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res_email = await request(app)
            .post('/account/refund')
            .send(webhookRequest);
        expect(res_email.status).to.equal(200);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(true);
        expect(res_email.body.message).to.equal('Deactivated');

        const res_email_1 = await request(app)
            .post('/account/refund')
            .send(webhookRequest);
        expect(res_email_1.status).to.equal(400);
    });

    it('should error when refund account does not exist', async () => {
        const res_email = await request(app)
            .post('/account/refund')
            .send(webhookRequest);
        expect(res_email.status).to.equal(404);
        expect(res_email.body).not.to.be.empty;
        expect(res_email.body.success).to.equal(false);
        expect(res_email.body.error).to.equal('Account not found');
    });
});

describe('Accounts - /account/login', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should login', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res = await request(app).post('/account/login').send({
            email: webhookRequest.customer.email,
            password: password,
        });
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message).to.be.not.null;
    });

    it('should fail when invalid username password is sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res = await request(app).post('/account/login').send({
            email: webhookRequest.customer.email,
            password: 'heyjfdklf',
        });
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.equal('Email or password did not match');
    });
});

describe('Accounts - /account/password', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should update password for the user', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .patch('/account/password')
            .set('authorization', token)
            .send({
                password: 'heyjfdklf',
            });
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(true);
        expect(res.body.message.status).to.equal('ACTIVE');
    });

    it('should fail when invalid token is sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res = await request(app)
            .patch('/account/password')
            .set('authorization', 'test')
            .send({
                password: 'heyjfdklf',
            });
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when token is not sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const res = await request(app).patch('/account/password').send({
            password: 'heyjfdklf',
        });
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Accounts - /account/deactivate', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should deactivate the user', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/account/deactivate')
            .set('authorization', token);
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
        expect(res.body.message).to.equal('Deactivated');

        return res.body.message.id;
    });

    it('should fail whe the user is already deactivated', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/account/deactivate')
            .set('authorization', token);
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
        expect(res.body.message).to.equal('Deactivated');
        const res_1 = await request(app)
            .post('/account/deactivate')
            .set('authorization', token);
        expect(res_1.status).to.equal(400);
    });

    it('should fail when invalid token is sent', async () => {
        const res = await request(app)
            .post('/account/deactivate')
            .set('authorization', 'test');
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when token is not sent', async () => {
        const res = await request(app).post('/account/deactivate');
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Invite - /account/invite', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should fail when invalid account_type is sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/account/invite')
            .set('authorization', token)
            .send({
                email: 'test@gmail.com',
                account_type_id: 1100000000,
                first_name: 'Test',
                last_name: 'User',
            });
        expect(res.status).to.equal(400);
    });

    it('should fail when the user is not activated', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/account/invite')
            .set('authorization', token)
            .send({
                email: webhookRequest.customer.email,
                account_type_id: 1,
                first_name: webhookRequest.customer.first_name,
                last_name: webhookRequest.customer.first_name,
            });
        expect(res.status).to.equal(400);
    });

    it('should fail when invalid token is sent', async () => {
        const res = await request(app)
            .post('/account/invite')
            .set('authorization', 'token')
            .send({
                email: webhookRequest.customer.email,
                account_type_id: 1,
                first_name: webhookRequest.customer.first_name,
                last_name: webhookRequest.customer.first_name,
            });
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when token is not sent', async () => {
        const res = await request(app).post('/account/invite').send({
            email: webhookRequest.customer.email,
            account_type_id: 1,
            first_name: webhookRequest.customer.first_name,
            last_name: webhookRequest.customer.first_name,
        });
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Downloads - /download', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should make an entry in download', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/download')
            .send({
                url: 'https://s3.com',
            })
            .set('authorization', token);
        expect(res.status).to.equal(201);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(true);
        expect(res.body.message.id).to.be.not.null;
    });

    it('should fail when invalid token is sent', async () => {
        const res = await request(app)
            .post('/download')
            .set('authorization', 'test');
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when no token is not sent', async () => {
        const res = await request(app).post('/download');
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when body is not sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        const res = await request(app)
            .post('/download')
            .set('authorization', token);
        expect(res.status).to.equal(400);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.be.not.null;
    });

    it('should return count of download', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();
        await createDownload(token);

        const res = await request(app)
            .get('/download')
            .set('authorization', token);
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(true);
        expect(res.body.message.count).to.equal(1);
    });

    it('should return count of download as 0', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        const token = await login();

        const res = await request(app)
            .get('/download')
            .set('authorization', token);
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(true);
        expect(res.body.message.count).to.equal(0);
    });

    it('should fail when invalid token is sent', async () => {
        const userId = await createAccount();
        await activateAccount(userId);
        await login();
        const res = await request(app)
            .get('/download')
            .set('authorization', 'token');
        expect(res.status).to.equal(401);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.equal(false);
        expect(res.body.error).to.not.be.null;
    });
});
