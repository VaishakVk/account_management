/* eslint-disable max-lines */
import * as dotenv from 'dotenv';
dotenv.config();

import { agent as request } from 'supertest';
import * as sinon from 'sinon';
import * as mysql from 'mysql';
import MySqlDatabase from '../database/connect';
import { MailService } from '../services/utils/mail';
import { createUserAndLogin } from './account.spec';
import { expect } from 'chai';
import app from '../app';
let stubMail;

const cleanup = () =>
    new Promise(async (resolve, reject) => {
        const connection: mysql.Connection = await MySqlDatabase.getConnection();
        connection.query('DELETE FROM downloads', (err1) => {
            if (err1) reject(err1);
            // connection.query(
            //     'UPDATE accounts SET parent_id = null, account_type_id = null',
            //     (err2) => {
            //         if (err2) reject(err2);
            connection.query(
                `SET foreign_key_checks = 0; 
                DELETE FROM account_type_x_account_type; 
                SET foreign_key_checks = 1;`,
                (err4) => {
                    if (err4) reject(err4);
                    connection.query(
                        `SET foreign_key_checks = 0; 
                        DELETE FROM account_types WHERE name NOT IN ('enterprise', 'solo'); 
                        SET foreign_key_checks = 1;`,
                        (err3) => {
                            if (err3) reject(err3);
                            connection.query(
                                'SET foreign_key_checks = 0; DELETE FROM accounts; SET foreign_key_checks = 1;',
                                (err, res) => {
                                    if (err) reject(err);
                                    else resolve(res);
                                }
                            );
                        }
                    );
                }
            );
        });
    });
// });

const createAccountType = async () => {
    const { token, userId } = await createUserAndLogin();
    const res = await request(app)
        .post('/accountType')
        .set('authorization', token)
        .send({ name: 'TestAcc' });
    expect(res.status).to.equal(201);
    expect(res.body).not.to.be.empty;
    expect(res.body.success).to.be.true;
    expect(res.body.message.id).to.be.not.null;
    return { token, userId, accountTypeId: res.body.message.id };
};

describe('Accounts - /accountType', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should create a new account type', async () => {
        const { token } = await createUserAndLogin();
        const res = await request(app)
            .post('/accountType')
            .set('authorization', token)
            .send({ name: 'TestAcc' });
        expect(res.status).to.equal(201);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
    });

    it('should error when duplicate account type is created', async () => {
        const { token } = await createUserAndLogin();
        const res = await request(app)
            .post('/accountType')
            .set('authorization', token)
            .send({ name: 'TestAcc' });
        expect(res.status).to.equal(201);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
        const res_dup = await request(app)
            .post('/accountType')
            .set('authorization', token)
            .send({ name: 'TestAcc' });
        expect(res_dup.status).to.equal(400);
        expect(res_dup.body.error).to.be.not.null;
    });

    it('should update account type', async () => {
        const { token, accountTypeId } = await createAccountType();
        const res = await request(app)
            .patch('/accountType')
            .set('authorization', token)
            .send({ id: accountTypeId, name: 'Test' });
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.message.name).to.equal('Test');
    });

    it('should fail when token is not sent', async () => {
        const res = await request(app)
            .patch('/accountType')
            .send({ id: 948938, name: 'Test' });
        expect(res.status).to.equal(401);
        expect(res.body.error).to.be.not.null;
    });

    it('should fail when token is not sent', async () => {
        const res = await request(app)
            .post('/accountType')
            .send({ id: 948938, name: 'Test' });
        expect(res.status).to.equal(401);
        expect(res.body.error).to.be.not.null;
    });
});

describe('Link Account - /account/link', () => {
    before(function () {
        const obj: Promise<void> = new Promise((resolve) => {
            resolve();
        });
        stubMail = sinon.stub(MailService, 'sendEmail').returns(obj);
    });
    after(async function () {
        await cleanup();
        stubMail.restore();
    });
    beforeEach(() => cleanup());

    it('should create a new account mapping', async () => {
        const { token, userId, accountTypeId } = await createAccountType();
        const res = await request(app)
            .post('/account/link')
            .set('authorization', token)
            .send({
                user_id: userId,
                account_type_id: accountTypeId,
            });
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.success).to.be.true;
        expect(res.body.message.id).to.be.not.null;
        expect(res.body.message.account_type_id).to.equal(accountTypeId);
    });
});
