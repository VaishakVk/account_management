export interface InterfaceAccount {
    id?: number;
    email: string;
    password?: string;
    first_name?: string;
    last_name?: string;
    status: string;
    token?: string;
    expires_at?: Date;
    forget_password_token?: string;
    forget_password_token_expiry?: Date;
    activated_at?: Date;
    last_logged_in?: Date;
    webhook_response?: string;
    parent_id?: number;
    parent_account_type_id?: number;
    account_type_id?: number;
    invite_code?: string;
    invite_expiry?: Date;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
    parent_name?: string;
    parent_status?: string;
    parent_account?: string;
    child_account?: string;
}
