export interface InterfaceDownloads {
    id?: number;
    user_id: number;
    url_downloaded: string;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}
