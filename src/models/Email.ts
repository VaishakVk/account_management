interface verification {
    code: string;
    url: string;
}

export interface EmailVerificationTemplate {
    name: string;
    verification: verification;
}
export interface EmailInviteTemplate {
    name: string;
    invite: verification;
}

export interface EmailResetPasswordTemplate {
    name: string;
    url: string;
}

export interface EmailNameTemplate {
    name: string;
}
