export interface InterfaceAccountMapping {
    id?: number;
    parent_id: number;
    child_id: number;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}
