export enum StatusCode {
    Created = 201,
    Ok = 200,
    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    InternalServerError = 500,
}

export enum AccountStatusEnum {
    inactive = 'INACTIVE',
    active = 'ACTIVE',
}

export enum EmailTemplates {
    emailVerification = 'd-056806ef7fec4b0b95181d35b9273415',
    resetPassword = 'd-b8b68cac4eb14fa0bb1bfa2d3421a723',
    welcome = 'd-4372c5e74b4c459d8afce8399c9a2932',
    deactivated = 'd-1c1c78fb311c43efbb62e15108c4d9ba',
    invite = 'd-bd35efc803254c00b283082796732c14',
}

export const externalUrl = 'https://www.vanspace3d.co/';
export const enterprise = 'enterprise';
export const solo = 'solo';
